import org.junit.Test

class HelloTest {
    @Test
    fun testHello() {
        hello() should_be "hello world"
    }
}
