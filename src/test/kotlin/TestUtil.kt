import org.junit.Assert

infix fun <T> T.should_be(expected: T) {
    Assert.assertEquals(this, expected)
}
